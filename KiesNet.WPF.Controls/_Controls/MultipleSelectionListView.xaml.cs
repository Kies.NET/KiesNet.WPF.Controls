﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using KiesNet.MVVM;

namespace KiesNet.WPF.Controls
{
    /// <summary>
    /// Interaktionslogik für MutlipleSelectionListView.xaml
    /// </summary>
    public partial class MultipleSelectionListView : ListView
    {
        public event EventHandler<MouseButtonEventArgs> ItemDoubleClick;

        public new IEnumerable<SelectableItem> ItemsSource
        {
            get => (IEnumerable<SelectableItem>)this.GetValue(ItemsSourceProperty);
            set => this.SetValue(ItemsSourceProperty, value);
        }

        public MultipleSelectionListView()
        {
            this.InitializeComponent();
        } 

        private void OnListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs args)
        {
            this.ItemDoubleClick?.Invoke(sender, args);
            args.Handled = true;
        }
    }
}
