﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KiesNet.MVVM;

namespace KiesNet.WPF.Controls
{
    public class SelectableItem : ViewModelBase
    {
        public bool IsSelected
        {
            get => this.isSelected;
            set
            {
                this.isSelected = value;
                this.OnPropertyChanged();
            }
        }

        public string Title
        {
            get => this.title;
            set
            {
                this.title = value;
                this.OnPropertyChanged();
            }
        }

        public string Description
        {
            get => this.description;
            set
            {
                this.description = value;
                this.OnPropertyChanged();
            }
        }

        private bool isSelected;
        private string title;
        private string description = string.Empty;

        public override string ToString() => this.Title;
    }
}
