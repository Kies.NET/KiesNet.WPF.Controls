﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KiesNet.WPF.Controls
{
    public class SelectableItem<T> : SelectableItem
    {
        public T Item { get; set; }

        public SelectableItem(T item)
        {
            this.Item = item;

            this.Title = this.Item.ToString();
        }
    }
}
