﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Threading;

namespace KiesNet.WPF.Controls
{
    public class ClickBehavior : Behavior<FrameworkElement>
    {
        public static DependencyProperty ClickCommandProperty = DependencyProperty.Register(nameof(ClickCommand), typeof(ICommand), typeof(ClickBehavior));
        public static DependencyProperty DoubleClickCommandProperty = DependencyProperty.Register(nameof(DoubleClickCommand), typeof(ICommand), typeof(ClickBehavior));

        public ICommand ClickCommand
        {
            get => (ICommand)this.GetValue(ClickCommandProperty);
            set => this.SetValue(ClickCommandProperty, value);
        }

        public ICommand DoubleClickCommand
        {
            get => (ICommand)this.GetValue(DoubleClickCommandProperty);
            set => this.SetValue(DoubleClickCommandProperty, value);
        }

        private DispatcherTimer dispatcherTimer;

        protected override void OnAttached()
        {
            this.dispatcherTimer = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(200)
            };
            this.dispatcherTimer.Tick += this.OnDispatcherTimer_Tick;

            this.AssociatedObject.PreviewMouseLeftButtonDown += this.OnAssociatedObject_PreviewMouseLeftButtonDown;
        }

        protected override void OnDetaching()
        {
            this.dispatcherTimer.Tick -= this.OnDispatcherTimer_Tick;
            this.dispatcherTimer = null;

            this.AssociatedObject.PreviewMouseLeftButtonDown -= this.OnAssociatedObject_PreviewMouseLeftButtonDown;
        }

        private void OnDispatcherTimer_Tick(object sender, EventArgs e)
        {
            this.dispatcherTimer.Stop();
            this.ClickCommand?.Execute(sender);
        }

        private void OnAssociatedObject_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                this.dispatcherTimer.Stop();
                this.DoubleClickCommand?.Execute(sender);
            }
            else
                this.dispatcherTimer.Start();
        }
    }
}
