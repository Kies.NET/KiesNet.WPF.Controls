﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace KiesNet.WPF.Controls
{
    internal class CustomAttachedBehavior
    {
        public static readonly DependencyProperty BehaviorProperty
            = DependencyProperty.RegisterAttached("Behavior", typeof(Behavior), typeof(CustomAttachedBehavior), new FrameworkPropertyMetadata(null, OnBehaviorChanged));

        private static Task behaviorTask;

        public static void SetBehavior(UIElement element, Behavior value)
        {
            element.SetValue(BehaviorProperty, value);
        }

        public static Behavior GetBehavior(UIElement element)
        {
            return (Behavior) element.GetValue(BehaviorProperty);
        }
        
        private static void OnBehaviorChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            if (behaviorTask != null 
                    || dependencyPropertyChangedEventArgs.NewValue == null)
                return;

            behaviorTask = Task.Run(() =>
            {
                BehaviorCollection collection = null;
                bool collectionFrozen = true;

                dependencyObject.Dispatcher.Invoke(() =>
                {
                    collection = Interaction.GetBehaviors(dependencyObject);
                    collectionFrozen = collection.IsFrozen;
                });
                
                while (collectionFrozen)
                {
                    Thread.Sleep(500);
                    dependencyObject.Dispatcher.Invoke(() => collectionFrozen = collection.IsFrozen);
                }

                Behavior behavior = (Behavior)dependencyPropertyChangedEventArgs.NewValue;
                dependencyObject.Dispatcher.Invoke(() => collection.Add((Behavior)behavior.CloneCurrentValue()));

                behaviorTask = null;
            });
        }
    }
}
